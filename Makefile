all:
	python3 turn_hundred.py

test:
	python3 turn_hundred_test.py

clean:
	rm -f *~

.PHONY: all test clean
