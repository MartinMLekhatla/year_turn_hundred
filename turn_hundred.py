def get_name():
    name = input("Name > ")
    return name


def get_age():
    while True:
        try:
            age = int(input("Age > "))
            return age
        except ValueError:
            print("Invalid input. Please enter a number.")


def cal_year_turn_hundred(age):
    current_year = 2023
    year_turn_hundred = (100 - age) + current_year
    return year_turn_hundred
    

def print_results(name, year_turn_hundred):
    print(f"{name}, you will turn 100 in the year {year_turn_hundred}.")


def run_calculation():
    Names = get_name()
    years_old = get_age()
    turn_hundred = cal_year_turn_hundred(years_old)
    print_results(Names, turn_hundred)

  
if __name__ == "__main__":
    run_calculation()
