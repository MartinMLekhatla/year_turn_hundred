import unittest
import turn_hundred

class TestTurnHundred(unittest.TestCase):
    def test_cal_year_turn_hundred(self):
        self.assertEqual(turn_hundred.cal_year_turn_hundred(25), 2098)
        self.assertEqual(turn_hundred.cal_year_turn_hundred(35), 2088)

    def test_print_results(self):
        # Test that the output of print_results is correct
        import io
        import sys
        captured_output = io.StringIO()
        sys.stdout = captured_output
        turn_hundred.print_results("John", 2098)
        self.assertEqual(captured_output.getvalue().strip(), "John, you will turn 100 in the year 2098.")

if __name__ == '__main__':
    unittest.main()
